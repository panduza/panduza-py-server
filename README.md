# Panduza Python Server

## Getting Started

To install the server, you need:

- Python3
- Pip3

Then install required python packages

```bash
# Install tools
pip3 install django djangorestframework django-cors-headers requests
```

Finally start the server

```bash
# Start the server
python3 manage.py runserver
```

On your web browser, copy/paste this url **http://localhost:8000**

You should see the main interface of the server.
